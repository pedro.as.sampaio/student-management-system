# Student Management System Web API

## Description
This Student Management System Web API is designed to help me learn and solidify my knowledge of .NET.

## Installation
Not yet available for deployment. Work in progres.

## Roadmap
- Configure WebAPI (Q2 2024)
- Configure xUnit tests  (Q2 2024)
- Securing API   (Q2 2024)
- Add more domain features
(maybe add UI. if added UI, secure  UI)  (Q3 2024)


## Project status
In development.


