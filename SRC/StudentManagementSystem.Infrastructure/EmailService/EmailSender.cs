﻿using System.Net;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using StudentManagementSystem.Application.Interfaces.Email;
using StudentManagementSystem.Application.Models.Email;

namespace StudentManagementSystem.Infrastructure.EmailService;

public class EmailSender : IEmailSender
{
  public EmailSender(IOptions<EmailSettings> emailSettings)
  {
    EmailSettings = emailSettings.Value;
  }

  public EmailSettings EmailSettings { get; }

  public async Task<bool> SendEmail(EmailMessage email)
  {
    var client = new SendGridClient(EmailSettings.ApiKey);

    var to = new EmailAddress(email.To);

    var from = new EmailAddress
    {
      Email = EmailSettings.FromAddress,
      Name = EmailSettings.FromName
    };

    var message = MailHelper.CreateSingleEmail(from, to, email.Subject, email.Body, email.Body);

    var response = await client.SendEmailAsync(message);

    return response.StatusCode == HttpStatusCode.OK ||
           response.StatusCode == HttpStatusCode.Accepted;
  }
}