﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudentManagementSystem.Application.Interfaces.Email;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Models.Email;
using StudentManagementSystem.Infrastructure.EmailService;
using StudentManagementSystem.Infrastructure.Logging;

namespace StudentManagementSystem.Infrastructure;

public static class InfrastructureServiceRegistration
{
  public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
    IConfiguration configuration)
  {
    services.Configure<EmailSettings>(configuration.GetSection("EmailSettings"));
    services.AddTransient<IEmailSender, EmailSender>();
    services.AddScoped(typeof(IAppLogger<>), typeof(LoggerAdapter<>));

    return services;
  }
}