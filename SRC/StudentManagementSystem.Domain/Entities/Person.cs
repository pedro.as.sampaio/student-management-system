﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Person : BaseEntity
{
  public string PersonName { get; set; } = string.Empty;
  public string FamilyName { get; set; } = string.Empty;
  public DateTime DateOfBirth { get; set; }
  public string Email { get; set; } = string.Empty;

  public int? AddressId { get; set; }
  public virtual Address? Address { get; set; }
}