﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Discipline : BaseEntity
{
  public string Name { get; set; } = string.Empty;
  public string Subject { get; set; } = string.Empty;
  public int GradeLevel { get; set; }

  public virtual List<TeacherDiscipline>? TeachersDisciplines { get; set; }

  public virtual List<Schedule>? Schedules { get; set; }

  public virtual List<CourseDiscipline>? CourseDisciplines { get; set; }
}