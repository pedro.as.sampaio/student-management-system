﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Class : BaseEntity
{
  public string Name { get; set; } = string.Empty;

  public int CourseId { get; set; }
  public virtual Course? Course { get; set; }

  public virtual List<Student>? Students { get; set; }
  public virtual List<ClassDisciplineTeacher>? CourseDisciplines { get; set; }
  public virtual List<Schedule>? Schedules { get; set; }
}