﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Course : BaseEntity
{
  public int CourseId { get; set; }
  public string Name { get; set; } = string.Empty;

  public virtual List<CourseDiscipline>? CourseDisciplines { get; set; }
  public virtual List<Class>? Classs { get; set; }
}