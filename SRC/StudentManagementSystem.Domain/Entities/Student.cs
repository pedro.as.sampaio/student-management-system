﻿using System.ComponentModel.DataAnnotations;
using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Student : BaseEntity
{
  public int StudentId { get; set; }
  public int GradeLevel { get; set; }

  [DataType(DataType.Date)] public DateTime EnrollmentDate { get; set; }

  public int PersonId { get; set; }
  public virtual Person? Person { get; set; }
}