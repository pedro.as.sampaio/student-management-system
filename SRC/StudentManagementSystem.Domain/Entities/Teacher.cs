﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Teacher : BaseEntity
{
  public int PersonId { get; set; }
  public virtual Person? Person { get; set; }

  public virtual List<ClassDisciplineTeacher>? CourseDisciplines { get; set; }

  public virtual List<TeacherDiscipline>? TeachersDisciplines { get; set; }
}