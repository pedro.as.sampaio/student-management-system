﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Schedule : BaseEntity
{
  public DateTime Date { get; set; }

  /// <summary>
  ///   Minutes
  /// </summary>
  public int Duration { get; set; }

  public int ClassId { get; set; }
  public virtual Class? Class { get; set; }

  public int DisciplineId { get; set; }
  public virtual Discipline? Discipline { get; set; }

  public int TeacherId { get; set; }
  public virtual Teacher? Teacher { get; set; }
}