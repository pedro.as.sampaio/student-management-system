﻿namespace StudentManagementSystem.Domain.Entities;

public class CourseDiscipline
{
  public int CourseId { get; set; }
  public virtual Course? Course { get; set; }

  public int DisciplineId { get; set; }
  public virtual Discipline? Discipline { get; set; }

  public string Observations { get; set; } = string.Empty;
  public DateTime CreatedAt { get; set; }
  public DateTime UpdatedAt { get; set; }
  public string CreatedBy { get; set; } = string.Empty;
  public string UpdatedBy { get; set; } = string.Empty;
}