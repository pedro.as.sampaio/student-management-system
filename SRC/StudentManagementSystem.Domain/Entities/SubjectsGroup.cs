﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class SubjectsGroup : BaseEntity
{
  public string Name { get; set; } = string.Empty;

  public virtual List<Discipline>? Disciplines { get; set; }
}