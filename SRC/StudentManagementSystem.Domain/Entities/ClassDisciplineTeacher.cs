﻿namespace StudentManagementSystem.Domain.Entities;

public class ClassDisciplineTeacher
{
  public int ClassId { get; set; }
  public virtual Class? Class { get; set; }

  public int DisciplineId { get; set; }
  public virtual Discipline? Discipline { get; set; }

  public int TeacherId { get; set; }
  public virtual Teacher? Teacher { get; set; }

  public string Observations { get; set; } = string.Empty;
  public DateTime CreatedAt { get; set; }
  public DateTime UpdatedAt { get; set; }
  public string CreatedBy { get; set; } = string.Empty;
  public string UpdatedBy { get; set; } = string.Empty;
}