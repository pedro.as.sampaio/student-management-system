﻿namespace StudentManagementSystem.Domain.Entities;

public class TeacherDiscipline
{
  public int TeacherId { get; set; }
  public Teacher? Teacher { get; set; }

  public int DisciplineId { get; set; }
  public Discipline? Discipline { get; set; }

  public virtual List<Schedule>? Schedules { get; set; }

  public DateTime CreatedAt { get; set; }
  public DateTime UpdatedAt { get; set; }
}