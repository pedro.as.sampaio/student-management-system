﻿using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Domain.Entities;

public class Address : BaseEntity
{
  public string Street { get; set; } = string.Empty;
  public string City { get; set; } = string.Empty;
  public string State { get; set; } = string.Empty;
  public string PostalCode { get; set; } = string.Empty;
  public string Country { get; set; } = string.Empty;

  public virtual Person Person { get; set; } = null!;
}