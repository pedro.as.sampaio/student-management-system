﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Persistence.Configurations;

/// <summary>
///   https://www.bestrandoms.com/random-address-in-pt
/// </summary>
public class PersonConfiguration : IEntityTypeConfiguration<Person>
{
  public void Configure(EntityTypeBuilder<Person> modelBuilder)
  {
    modelBuilder.ToTable("Persons");
    modelBuilder.HasKey(person => person.Id);
    modelBuilder.Property(person => person.Id).ValueGeneratedOnAdd();

    modelBuilder.HasOne(person => person.Address)
      .WithOne(address => address.Person)
      .HasForeignKey<Person>(person => person.AddressId)
      .OnDelete(DeleteBehavior.Cascade);

    modelBuilder.Property(person => person.PersonName).HasMaxLength(100).IsRequired();
    modelBuilder.Property(person => person.FamilyName).HasMaxLength(100).IsRequired();
    modelBuilder.Property(person => person.DateOfBirth).IsRequired();
    modelBuilder.Property(person => person.Email).HasMaxLength(100).IsRequired();
    modelBuilder.Property(person => person.AddressId);


    modelBuilder.HasData
    (
      new Person
      {
        Id = 1,
        PersonName = "John",
        FamilyName = "Doe",
        DateOfBirth = new DateTime(1990, 1, 1, 0, 0, 0, DateTimeKind.Utc),
        Email = "john.doe@example.com",
        AddressId = 1,
        CreatedAt = DateTime.UtcNow,
        UpdatedAt = DateTime.UtcNow
      },
      new Person
      {
        Id = 2,
        PersonName = "Jane",
        FamilyName = "Smith",
        DateOfBirth = new DateTime(1985, 5, 15, 0, 0, 0, DateTimeKind.Utc),
        Email = "jane.smith@example.com",
        AddressId = 2,
        CreatedAt = DateTime.UtcNow,
        UpdatedAt = DateTime.UtcNow
      }
    );
  }
}