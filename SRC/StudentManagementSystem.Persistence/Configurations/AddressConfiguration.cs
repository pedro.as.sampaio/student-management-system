﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Persistence.Configurations;

/// <summary>
///   https://www.bestrandoms.com/random-address-in-pt
/// </summary>
public class AddressConfiguration : IEntityTypeConfiguration<Address>
{
  public void Configure(EntityTypeBuilder<Address> modelBuilder)
  {
    modelBuilder.ToTable("Addresses");
    modelBuilder.HasKey(address => address.Id);
    modelBuilder.Property(address => address.Id).ValueGeneratedOnAdd();

    modelBuilder.Property(address => address.Street).HasMaxLength(100).IsRequired();
    modelBuilder.Property(address => address.City).HasMaxLength(100).IsRequired();
    modelBuilder.Property(address => address.State).HasMaxLength(100).IsRequired();
    modelBuilder.Property(address => address.PostalCode).HasMaxLength(20).IsRequired();
    modelBuilder.Property(address => address.Country).HasMaxLength(100).IsRequired();
    modelBuilder.Property(address => address.CreatedAt).IsRequired();
    modelBuilder.Property(address => address.UpdatedAt).IsRequired();

    modelBuilder.HasData
    (
      new Address
      {
        Id = 1,
        Street = "123 Main St",
        City = "Anytown",
        State = "Anystate",
        PostalCode = "12345",
        Country = "USA",
        CreatedAt = DateTime.UtcNow,
        UpdatedAt = DateTime.UtcNow
      },
      new Address
      {
        Id = 2,
        Street = "456 Elm St",
        City = "Othertown",
        State = "Otherstate",
        PostalCode = "67890",
        Country = "USA",
        CreatedAt = DateTime.UtcNow,
        UpdatedAt = DateTime.UtcNow
      }
    );
  }
}