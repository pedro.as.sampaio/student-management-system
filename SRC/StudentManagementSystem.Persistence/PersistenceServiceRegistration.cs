﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Application.Interfaces.Persistance.Common;
using StudentManagementSystem.Persistence.DatabaseContext;
using StudentManagementSystem.Persistence.Repositories;

namespace StudentManagementSystem.Persistence;

public static class PersistenceServiceRegistration
{
  public static IServiceCollection AddPersistenceServices(this IServiceCollection services,
    IConfiguration configuration)
  {
    services.AddDbContext<StudentsDbContext>(options =>
    {
      options.UseSqlServer(configuration.GetConnectionString("StudentsDbCs"));
    });

    services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
    services.AddScoped<IAddressRepository, AddressRepository>();
    services.AddScoped<IPersonRepository, PersonRepository>();

    return services;
  }
}