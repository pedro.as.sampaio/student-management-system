﻿using Microsoft.EntityFrameworkCore;
using StudentManagementSystem.Application.Interfaces.Persistance.Common;
using StudentManagementSystem.Domain.Entities.Common;
using StudentManagementSystem.Persistence.DatabaseContext;

namespace StudentManagementSystem.Persistence.Repositories;

public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity, new()
{
  protected readonly StudentsDbContext Context;

  public GenericRepository(StudentsDbContext context)
  {
    Context = context;
  }

  public async Task<IReadOnlyList<T>> GetAsync()
  {
    return await Context.Set<T>()
      .AsNoTracking()
      .ToListAsync()
      .ConfigureAwait(false);
  }

  public async Task<T?> GetByIdAsync(int id)
  {
    return await Context.Set<T>()
      .AsNoTracking()
      .FirstOrDefaultAsync(options => options.Id == id)
      .ConfigureAwait(false);
  }

  public async Task<T> CreateAsync(T entity)
  {
    await Context.AddAsync(entity).ConfigureAwait(false);
    await Context.SaveChangesAsync().ConfigureAwait(false);
    return entity;
  }

  public async Task<T> UpdateAsync(T entity)
  {
    Context.Update(entity);
    Context.Entry(entity).State = EntityState.Modified;
    await Context.SaveChangesAsync().ConfigureAwait(false);
    return entity;
  }

  public async Task Delete(int id)
  {
    var entity = new T() { Id = id };
    Context.Entry(entity).State = EntityState.Deleted;
    await Context.SaveChangesAsync().ConfigureAwait(false);
  }
}