﻿using Microsoft.EntityFrameworkCore;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;
using StudentManagementSystem.Persistence.DatabaseContext;

namespace StudentManagementSystem.Persistence.Repositories;

public class PersonRepository : GenericRepository<Person>, IPersonRepository
{
  public PersonRepository(StudentsDbContext context) : base(context)
  {
  }

  public async Task<bool> IsUniqueEmailAsync(string email, CancellationToken token)
  {
    return await Context.Persons.AnyAsync(person => person.Email != email, token);
  }
}