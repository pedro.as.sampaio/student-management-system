﻿using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;
using StudentManagementSystem.Persistence.DatabaseContext;

namespace StudentManagementSystem.Persistence.Repositories;

public class AddressRepository : GenericRepository<Address>, IAddressRepository
{
  public AddressRepository(StudentsDbContext context) : base(context)
  {
  }
}