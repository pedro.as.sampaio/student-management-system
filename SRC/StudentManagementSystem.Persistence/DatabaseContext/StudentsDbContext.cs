﻿using Microsoft.EntityFrameworkCore;
using StudentManagementSystem.Domain.Entities;
using StudentManagementSystem.Domain.Entities.Common;

namespace StudentManagementSystem.Persistence.DatabaseContext;

public class StudentsDbContext : DbContext
{
  public StudentsDbContext(DbContextOptions<StudentsDbContext> options) : base(options)
  {
  }

  public DbSet<Address> Addresses { get; set; }
  public DbSet<Person> Persons { get; set; }

  protected override void OnModelCreating(ModelBuilder modelBuilder)
  {
    modelBuilder.ApplyConfigurationsFromAssembly(typeof(StudentsDbContext).Assembly);

    base.OnModelCreating(modelBuilder);
  }

  public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
  {
    foreach (var baseEntry in base.ChangeTracker.Entries<BaseEntity>()
               .Where(entityEntry => entityEntry.State is EntityState.Added or EntityState.Modified))
    {
      baseEntry.Entity.UpdatedAt = DateTime.Now;

      if (baseEntry.State is EntityState.Added) baseEntry.Entity.CreatedAt = DateTime.Now;
    }

    return base.SaveChangesAsync(cancellationToken);
  }
}