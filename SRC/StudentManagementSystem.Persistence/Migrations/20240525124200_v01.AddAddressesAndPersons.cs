﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StudentManagementSystem.Persistence.Migrations
{
    /// <inheritdoc />
    public partial class v01AddAddressesAndPersons : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Street = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    City = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    State = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PostalCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Country = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PersonName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    FamilyName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    AddressId = table.Column<int>(type: "int", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Persons_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "City", "Country", "CreatedAt", "PostalCode", "State", "Street", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, "Anytown", "USA", new DateTime(2024, 5, 25, 12, 42, 0, 72, DateTimeKind.Utc).AddTicks(8327), "12345", "Anystate", "123 Main St", new DateTime(2024, 5, 25, 12, 42, 0, 72, DateTimeKind.Utc).AddTicks(8328) },
                    { 2, "Othertown", "USA", new DateTime(2024, 5, 25, 12, 42, 0, 72, DateTimeKind.Utc).AddTicks(8329), "67890", "Otherstate", "456 Elm St", new DateTime(2024, 5, 25, 12, 42, 0, 72, DateTimeKind.Utc).AddTicks(8330) }
                });

            migrationBuilder.InsertData(
                table: "Persons",
                columns: new[] { "Id", "AddressId", "CreatedAt", "DateOfBirth", "Email", "FamilyName", "PersonName", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2024, 5, 25, 12, 42, 0, 73, DateTimeKind.Utc).AddTicks(946), new DateTime(1990, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc), "john.doe@example.com", "Doe", "John", new DateTime(2024, 5, 25, 12, 42, 0, 73, DateTimeKind.Utc).AddTicks(946) },
                    { 2, 2, new DateTime(2024, 5, 25, 12, 42, 0, 73, DateTimeKind.Utc).AddTicks(948), new DateTime(1985, 5, 15, 0, 0, 0, 0, DateTimeKind.Utc), "jane.smith@example.com", "Smith", "Jane", new DateTime(2024, 5, 25, 12, 42, 0, 73, DateTimeKind.Utc).AddTicks(948) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Persons_AddressId",
                table: "Persons",
                column: "AddressId",
                unique: true,
                filter: "[AddressId] IS NOT NULL");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "Addresses");
        }
    }
}
