﻿using MediatR;

namespace StudentManagementSystem.Application.Features.Person.Commands.DeletePerson;

public record DeletePersonCommand : IRequest<Unit>
{
  public int Id { get; set; }
}