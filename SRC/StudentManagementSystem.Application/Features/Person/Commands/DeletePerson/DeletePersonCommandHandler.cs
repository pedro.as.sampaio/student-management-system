﻿using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Features.Person.Commands.DeletePerson;

public class DeletePersonCommandHandler : IRequestHandler<DeletePersonCommand, Unit>
{
  private readonly IAppLogger<DeletePersonCommandHandler> _logger;
  private readonly IPersonRepository _personRepository;

  public DeletePersonCommandHandler(IPersonRepository personRepository, IAppLogger<DeletePersonCommandHandler> logger)
  {
    _personRepository = personRepository;
    _logger = logger;
  }

  public async Task<Unit> Handle(DeletePersonCommand request, CancellationToken cancellationToken)
  {
    var person = await _personRepository.GetByIdAsync(request.Id);

    if (person is null)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(DeletePersonCommand), request);
      throw new NotFoundException(nameof(Address), request.Id);
    }

    await _personRepository.Delete(request.Id);

    return Unit.Value;
  }
}