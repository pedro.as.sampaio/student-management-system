﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Features.Person.Commands.UpdatePerson;

public class UpdatePersonCommandHandler : IRequestHandler<UpdatePersonCommand, Unit>
{
  private readonly IAppLogger<UpdatePersonCommandHandler> _logger;
  private readonly IMapper _mapper;
  private readonly IPersonRepository _personRepository;

  public UpdatePersonCommandHandler(IMapper mapper, IPersonRepository personRepository,
    IAppLogger<UpdatePersonCommandHandler> logger)
  {
    _mapper = mapper;
    _personRepository = personRepository;
    _logger = logger;
  }

  public async Task<Unit> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
  {
    var validator = new UpdatePersonCommandValidator(_personRepository);
    var validationResult = await validator.ValidateAsync(request, cancellationToken);

    if (!validationResult.IsValid)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(UpdatePersonCommand), request);
      throw new BadRequestException("Invalid data provided.", validationResult);
    }

    var person = await _personRepository.GetByIdAsync(request.Id);


    if (person is null)
      throw new NotFoundException(nameof(Address), request.Id);

    person = _mapper.Map<Domain.Entities.Person>(request);

    await _personRepository.UpdateAsync(person);

    return Unit.Value;
  }
}