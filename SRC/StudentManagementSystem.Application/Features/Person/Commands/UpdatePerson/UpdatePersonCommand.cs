﻿using MediatR;

namespace StudentManagementSystem.Application.Features.Person.Commands.UpdatePerson;

public record UpdatePersonCommand : IRequest<Unit>
{
  public int Id { get; set; }
  public string PersonName { get; set; } = string.Empty;
  public string FamilyName { get; set; } = string.Empty;
  public DateTime DateOfBirth { get; set; }
  public string Email { get; set; } = string.Empty;
}