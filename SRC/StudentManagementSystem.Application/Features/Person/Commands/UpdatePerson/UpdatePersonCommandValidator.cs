﻿using FluentValidation;
using StudentManagementSystem.Application.Interfaces.Persistance;

namespace StudentManagementSystem.Application.Features.Person.Commands.UpdatePerson;

public class UpdatePersonCommandValidator : AbstractValidator<UpdatePersonCommand>
{
  private readonly IPersonRepository _personRepository;

  public UpdatePersonCommandValidator(IPersonRepository personRepository)
  {
    _personRepository = personRepository;

    RuleFor(entity => entity.PersonName)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull()
      .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

    RuleFor(entity => entity.FamilyName)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull()
      .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

    RuleFor(entity => entity.DateOfBirth)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull();

    RuleFor(entity => entity.Email)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull()
      .EmailAddress()
      .MaximumLength(100).WithMessage("{PropertyName} must not exceed 100 characters.");

    RuleFor(entity => entity)
      .MustAsync(PersonTypeEmailUnique)
      .WithMessage("A person with the same email already exists.");
  }

  private Task<bool> PersonTypeEmailUnique(UpdatePersonCommand command, CancellationToken token)
  {
    return _personRepository.IsUniqueEmailAsync(command.Email, token);
  }
}