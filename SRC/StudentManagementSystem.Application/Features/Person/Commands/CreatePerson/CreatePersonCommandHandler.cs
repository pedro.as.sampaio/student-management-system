﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;

namespace StudentManagementSystem.Application.Features.Person.Commands.CreatePerson;

public class CreatePersonCommandHandler : IRequestHandler<CreatePersonCommand, int>
{
  private readonly IAppLogger<CreatePersonCommandHandler> _logger;
  private readonly IMapper _mapper;
  private readonly IPersonRepository _personRepository;

  public CreatePersonCommandHandler(IMapper mapper, IPersonRepository personRepository,
    IAppLogger<CreatePersonCommandHandler> logger)
  {
    _mapper = mapper;
    _personRepository = personRepository;
    _logger = logger;
  }

  public async Task<int> Handle(CreatePersonCommand request, CancellationToken cancellationToken)
  {
    var validator = new CreatePersonCommandValidator(_personRepository);
    var validationResult = await validator.ValidateAsync(request, cancellationToken);

    if (!validationResult.IsValid)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(CreatePersonCommand), request);
      throw new BadRequestException("Invalid data provided.", validationResult);
    }

    var person = _mapper.Map<Domain.Entities.Person>(request);

    await _personRepository.CreateAsync(person);

    return person.Id;
  }
}