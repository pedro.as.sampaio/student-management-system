﻿using MediatR;

namespace StudentManagementSystem.Application.Features.Person.Commands.CreatePerson;

public record CreatePersonCommand : IRequest<int>
{
  public string PersonName { get; set; } = string.Empty;
  public string FamilyName { get; set; } = string.Empty;
  public DateTime DateOfBirth { get; set; }
  public string Email { get; set; } = string.Empty;
}