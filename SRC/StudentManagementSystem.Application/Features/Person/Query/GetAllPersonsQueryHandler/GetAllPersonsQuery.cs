﻿using MediatR;

namespace StudentManagementSystem.Application.Features.Person.Query.GetAllPersonsQueryHandler;

public record GetAllPersonsQuery : IRequest<List<PersonDto>>;