﻿namespace StudentManagementSystem.Application.Features.Person.Query.GetAllPersonsQueryHandler;

public class PersonDto
{
  public int Id { get; init; }
  public string PersonName { get; set; } = string.Empty;
  public string FamilyName { get; set; } = string.Empty;
  public DateTime DateOfBirth { get; set; }
  public string Email { get; set; } = string.Empty;
}