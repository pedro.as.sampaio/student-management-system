﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Interfaces.Persistance;

namespace StudentManagementSystem.Application.Features.Person.Query.GetAllPersonsQueryHandler;

public class GetAllPersonsQueryHandler : IRequestHandler<GetAllPersonsQuery, List<PersonDto>>
{
  private readonly IMapper _mapper;
  private readonly IPersonRepository _personRepository;

  public GetAllPersonsQueryHandler(IMapper mapper, IPersonRepository personRepository)
  {
    _mapper = mapper;
    _personRepository = personRepository;
  }

  public async Task<List<PersonDto>> Handle(GetAllPersonsQuery request, CancellationToken cancellationToken)
  {
    var personList = await _personRepository.GetAsync();

    var result = _mapper.Map<List<PersonDto>>(personList);

    return result;
  }
}