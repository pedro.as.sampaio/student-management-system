﻿namespace StudentManagementSystem.Application.Features.Person.Query.GetPersonQueryHandler;

public class PersonDetailDto
{
  public string PersonName { get; set; } = string.Empty;
  public string FamilyName { get; set; } = string.Empty;
  public DateTime DateOfBirth { get; set; }
  public string Email { get; set; } = string.Empty;
  public int AddressId { get; set; }
  public int Id { get; set; }
  public string Observations { get; set; } = string.Empty;
  public DateTime CreatedAt { get; set; }
  public DateTime UpdatedAt { get; set; }
}