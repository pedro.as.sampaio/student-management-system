﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;

namespace StudentManagementSystem.Application.Features.Person.Query.GetPersonQueryHandler;

public class GetPersonQueryHandler : IRequestHandler<GetPersonQuery, PersonDetailDto>
{
  private readonly IAppLogger<GetPersonQueryHandler> _logger;
  private readonly IMapper _mapper;
  private readonly IPersonRepository _personRepository;

  public GetPersonQueryHandler(IMapper mapper, IPersonRepository personRepository,
    IAppLogger<GetPersonQueryHandler> logger)
  {
    _mapper = mapper;
    _personRepository = personRepository;
    _logger = logger;
  }


  public async Task<PersonDetailDto> Handle(GetPersonQuery request, CancellationToken cancellationToken)
  {
    var person = await _personRepository.GetByIdAsync(request.Id);

    if (person is null)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(GetPersonQuery), request);
      throw new NotFoundException(nameof(Person), request.Id);
    }

    var result = _mapper.Map<PersonDetailDto>(person);

    return result;
  }
}