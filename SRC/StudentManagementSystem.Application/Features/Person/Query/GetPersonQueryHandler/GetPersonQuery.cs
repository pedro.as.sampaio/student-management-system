﻿using MediatR;

namespace StudentManagementSystem.Application.Features.Person.Query.GetPersonQueryHandler;

public record GetPersonQuery(int Id) : IRequest<PersonDetailDto>;