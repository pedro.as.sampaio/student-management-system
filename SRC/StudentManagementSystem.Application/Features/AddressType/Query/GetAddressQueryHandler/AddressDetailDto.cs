﻿namespace StudentManagementSystem.Application.Features.AddressType.Query.GetAddressQueryHandler;

public class AddressDetailDto
{
  public string Street { get; set; } = string.Empty;
  public string City { get; set; } = string.Empty;
  public string State { get; set; } = string.Empty;
  public string PostalCode { get; set; } = string.Empty;
  public string Country { get; set; } = string.Empty;

  public int Id { get; set; }
  public string Observations { get; set; } = string.Empty;
  public DateTime CreatedAt { get; set; }
  public DateTime UpdatedAt { get; set; }
}