﻿using MediatR;

namespace StudentManagementSystem.Application.Features.AddressType.Query.GetAddressQueryHandler;

public record GetAddressQuery(int Id) : IRequest<AddressDetailDto>;