﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Features.AddressType.Query.GetAddressQueryHandler;

public class GetAddressQueryHandler : IRequestHandler<GetAddressQuery, AddressDetailDto>
{
  private readonly IAddressRepository _addressRepository;
  private readonly IAppLogger<GetAddressQueryHandler> _logger;
  private readonly IMapper _mapper;

  public GetAddressQueryHandler(IMapper mapper, IAddressRepository addressRepository,
    IAppLogger<GetAddressQueryHandler> logger)
  {
    _mapper = mapper;
    _addressRepository = addressRepository;
    _logger = logger;
  }


  public async Task<AddressDetailDto> Handle(GetAddressQuery request, CancellationToken cancellationToken)
  {
    var address = await _addressRepository.GetByIdAsync(request.Id);

    if (address is null)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(GetAddressQuery), request);
      throw new NotFoundException(nameof(Address), request.Id);
    }

    var result = _mapper.Map<AddressDetailDto>(address);

    return result;
  }
}