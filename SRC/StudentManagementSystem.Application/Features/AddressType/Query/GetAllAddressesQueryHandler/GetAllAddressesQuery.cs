﻿using MediatR;

namespace StudentManagementSystem.Application.Features.AddressType.Query.GetAllAddressesQueryHandler;

public record GetAllAddressesQuery : IRequest<List<AddressDto>>;