﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Interfaces.Persistance;

namespace StudentManagementSystem.Application.Features.AddressType.Query.GetAllAddressesQueryHandler;

public class GetAllAddressesQueryHandler : IRequestHandler<GetAllAddressesQuery, List<AddressDto>>
{
  private readonly IAddressRepository _addressRepository;
  private readonly IMapper _mapper;

  public GetAllAddressesQueryHandler(IMapper mapper, IAddressRepository addressRepository)
  {
    _mapper = mapper;
    _addressRepository = addressRepository;
  }

  public async Task<List<AddressDto>> Handle(GetAllAddressesQuery request, CancellationToken cancellationToken)
  {
    var addressList = await _addressRepository.GetAsync();

    var result = _mapper.Map<List<AddressDto>>(addressList);

    return result;
  }
}