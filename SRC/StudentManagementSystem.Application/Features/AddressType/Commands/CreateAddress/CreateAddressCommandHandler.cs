﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Features.AddressType.Commands.CreateAddress;

public class CreateAddressCommandHandler : IRequestHandler<CreateAddressCommand, int>
{
  private readonly IAddressRepository _addressRepository;
  private readonly IAppLogger<CreateAddressCommandHandler> _logger;
  private readonly IMapper _mapper;

  public CreateAddressCommandHandler(IMapper mapper, IAddressRepository addressRepository,
    IAppLogger<CreateAddressCommandHandler> logger)
  {
    _mapper = mapper;
    _addressRepository = addressRepository;
    _logger = logger;
  }

  public async Task<int> Handle(CreateAddressCommand request, CancellationToken cancellationToken)
  {
    var validator = new CreateAddressCommandValidator();
    var validationResult = await validator.ValidateAsync(request, cancellationToken);

    if (!validationResult.IsValid)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(CreateAddressCommand), request);
      throw new BadRequestException("Invalid data provided.", validationResult);
    }

    var address = _mapper.Map<Address>(request);

    await _addressRepository.CreateAsync(address);

    return address.Id;
  }
}