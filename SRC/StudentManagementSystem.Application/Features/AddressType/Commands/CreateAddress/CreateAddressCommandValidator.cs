﻿using FluentValidation;

namespace StudentManagementSystem.Application.Features.AddressType.Commands.CreateAddress;

public class CreateAddressCommandValidator : AbstractValidator<CreateAddressCommand>
{
  public CreateAddressCommandValidator()
  {
    RuleFor(entity => entity.Street)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull()
      .MaximumLength(50).WithMessage("{PropertyName} must not exceed 100 characters.");

    RuleFor(entity => entity.City)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull()
      .MaximumLength(50).WithMessage("{PropertyName} must not exceed 100 characters.");

    RuleFor(entity => entity.State)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull()
      .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

    RuleFor(entity => entity.Country)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .NotNull()
      .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

    RuleFor(entity => entity.PostalCode)
      .NotEmpty().WithMessage("{PropertyName} is required.")
      .Matches(@"^\d{4}(-\d{3})?$").WithMessage("{PropertyName} must be a valid Portuguese postal code.")
      .NotNull()
      .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
  }
}