﻿using AutoMapper;
using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Features.AddressType.Commands.UpdateAddress;

public class UpdateAddressCommandHandler : IRequestHandler<UpdateAddressCommand, Unit>
{
  private readonly IAddressRepository _addressRepository;
  private readonly IAppLogger<UpdateAddressCommandHandler> _logger;
  private readonly IMapper _mapper;

  public UpdateAddressCommandHandler(IMapper mapper, IAddressRepository addressRepository,
    IAppLogger<UpdateAddressCommandHandler> logger)
  {
    _mapper = mapper;
    _addressRepository = addressRepository;
    _logger = logger;
  }

  public async Task<Unit> Handle(UpdateAddressCommand request, CancellationToken cancellationToken)
  {
    var validator = new UpdateAddressCommandValidator();
    var validationResult = await validator.ValidateAsync(request, cancellationToken);

    if (!validationResult.IsValid)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(UpdateAddressCommand), request);
      throw new BadRequestException("Invalid data provided.", validationResult);
    }

    var address = await _addressRepository.GetByIdAsync(request.Id);


    if (address is null)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(UpdateAddressCommand), request);
      throw new NotFoundException(nameof(Address), request.Id);
    }

    address = _mapper.Map<Address>(request);

    await _addressRepository.UpdateAsync(address);

    return Unit.Value;
  }
}