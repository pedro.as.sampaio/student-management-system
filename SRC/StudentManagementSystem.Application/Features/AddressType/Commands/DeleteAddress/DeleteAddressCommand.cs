﻿using MediatR;

namespace StudentManagementSystem.Application.Features.AddressType.Commands.DeleteAddress;

public record DeleteAddressCommand : IRequest<Unit>
{
  public int Id { get; set; }
}