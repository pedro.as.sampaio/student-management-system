﻿using MediatR;
using StudentManagementSystem.Application.Exceptions;
using StudentManagementSystem.Application.Interfaces.Logging;
using StudentManagementSystem.Application.Interfaces.Persistance;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Features.AddressType.Commands.DeleteAddress;

public class DeleteAddressCommandHandler : IRequestHandler<DeleteAddressCommand, Unit>
{
  private readonly IAddressRepository _addressRepository;
  private readonly IAppLogger<DeleteAddressCommandHandler> _logger;

  public DeleteAddressCommandHandler(IAddressRepository addressRepository,
    IAppLogger<DeleteAddressCommandHandler> logger)
  {
    _addressRepository = addressRepository;
    _logger = logger;
  }

  public async Task<Unit> Handle(DeleteAddressCommand request, CancellationToken cancellationToken)
  {
    var address = await _addressRepository.GetByIdAsync(request.Id);

    if (address is null)
    {
      _logger.LogWarning("Validation errors - {0} - Request - {1}", nameof(DeleteAddressCommand), request);
      throw new NotFoundException(nameof(Address), request.Id);
    }

    await _addressRepository.Delete(request.Id);

    return Unit.Value;
  }
}