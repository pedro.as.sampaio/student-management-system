﻿using FluentValidation.Results;

namespace StudentManagementSystem.Application.Exceptions;

public class BadRequestException : Exception
{
  public BadRequestException(string message, ValidationResult validationResult) : base(message)
  {
    ValidationErrors = new List<string>();

    foreach (var validationFailure in validationResult.Errors) ValidationErrors.Add(validationFailure.ErrorMessage);
  }

  public List<string> ValidationErrors { get; set; }
}