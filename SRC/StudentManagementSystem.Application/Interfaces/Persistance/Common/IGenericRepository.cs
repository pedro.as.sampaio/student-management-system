﻿namespace StudentManagementSystem.Application.Interfaces.Persistance.Common;

public interface IGenericRepository<T>
{
  Task<IReadOnlyList<T>> GetAsync();
  Task<T?> GetByIdAsync(int id);
  Task<T> CreateAsync(T entity);
  Task<T> UpdateAsync(T entity);
  Task Delete(int id);
}