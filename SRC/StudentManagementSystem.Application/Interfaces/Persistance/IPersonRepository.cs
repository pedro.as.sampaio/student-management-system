﻿using StudentManagementSystem.Application.Interfaces.Persistance.Common;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Interfaces.Persistance;

public interface IPersonRepository : IGenericRepository<Person>
{
  Task<bool> IsUniqueEmailAsync(string email, CancellationToken token);
}