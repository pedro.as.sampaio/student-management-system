﻿using StudentManagementSystem.Application.Interfaces.Persistance.Common;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.Interfaces.Persistance;

public interface IAddressRepository : IGenericRepository<Address>
{
}