﻿using StudentManagementSystem.Application.Models.Email;

namespace StudentManagementSystem.Application.Interfaces.Email;

public interface IEmailSender
{
  Task<bool> SendEmail(EmailMessage email);
}