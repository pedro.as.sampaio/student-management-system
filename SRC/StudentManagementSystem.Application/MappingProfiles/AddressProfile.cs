﻿using AutoMapper;
using StudentManagementSystem.Application.Features.AddressType.Commands.CreateAddress;
using StudentManagementSystem.Application.Features.AddressType.Commands.UpdateAddress;
using StudentManagementSystem.Application.Features.AddressType.Query.GetAddressQueryHandler;
using StudentManagementSystem.Application.Features.AddressType.Query.GetAllAddressesQueryHandler;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.MappingProfiles;

public class AddressProfile : Profile
{
  public AddressProfile()
  {
    CreateMap<Address, AddressDto>().ReverseMap();
    CreateMap<Address, AddressDetailDto>().ReverseMap();
    CreateMap<Address, CreateAddressCommand>().ReverseMap();
    CreateMap<Address, UpdateAddressCommand>().ReverseMap();
  }
}