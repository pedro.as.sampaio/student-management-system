﻿using AutoMapper;
using StudentManagementSystem.Application.Features.Person.Commands.CreatePerson;
using StudentManagementSystem.Application.Features.Person.Commands.UpdatePerson;
using StudentManagementSystem.Application.Features.Person.Query.GetAllPersonsQueryHandler;
using StudentManagementSystem.Application.Features.Person.Query.GetPersonQueryHandler;
using StudentManagementSystem.Domain.Entities;

namespace StudentManagementSystem.Application.MappingProfiles;

public class PersonProfile : Profile
{
  public PersonProfile()
  {
    CreateMap<Person, PersonDto>().ReverseMap();
    CreateMap<Person, PersonDetailDto>().ReverseMap();
    CreateMap<Person, CreatePersonCommand>().ReverseMap();
    CreateMap<Person, UpdatePersonCommand>().ReverseMap();
    CreateMap<Person, PersonDetailDto>().ReverseMap();
  }
}