using StudentManagementSystem.Application;
using StudentManagementSystem.Infrastructure;
using StudentManagementSystem.Persistence;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddApplicationServices();
builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddPersistenceServices(builder.Configuration);
builder.Services.AddControllers();

builder.Services.AddCors(options =>
{
  options.AddPolicy("AllowAll", corsPolicyBuilder => corsPolicyBuilder
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());
});

// builder.Services.AddHealthChecks();
// builder.Services.AddHealthChecksUI();
// builder.Services.AddHealthChecks()
//   .AddDbContextCheck<StudentsDbContext>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();