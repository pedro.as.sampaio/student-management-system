using MediatR;
using Microsoft.AspNetCore.Mvc;
using StudentManagementSystem.Application.Features.Person.Commands.CreatePerson;
using StudentManagementSystem.Application.Features.Person.Commands.DeletePerson;
using StudentManagementSystem.Application.Features.Person.Commands.UpdatePerson;
using StudentManagementSystem.Application.Features.Person.Query.GetAllPersonsQueryHandler;
using StudentManagementSystem.Application.Features.Person.Query.GetPersonQueryHandler;

namespace StudentManagementSystem.API.Controllers;

[Route("api/[controller]")]
[ApiController]
[Produces("application/json")]
public class PersonsController : ControllerBase
{
  private readonly IMediator _mediator;

  public PersonsController(IMediator mediator)
  {
    _mediator = mediator;
  }

  // GET: api/<PersonsController>
  [HttpGet]
  [ProducesResponseType(typeof(List<PersonDto>), StatusCodes.Status200OK)]
  [ProducesResponseType(StatusCodes.Status400BadRequest)]
  public async Task<ActionResult<List<PersonDto>>> Get()
  {
    var persons = await _mediator.Send(new GetAllPersonsQuery());
    return StatusCode(200, persons);
  }

  // GET api/<PersonsController>/5
  [HttpGet("{id:int}")]
  [ProducesResponseType(typeof(PersonDto), StatusCodes.Status200OK)]
  [ProducesResponseType(StatusCodes.Status400BadRequest)]
  public async Task<ActionResult<PersonDetailDto>> Get(int id)
  {
    var person = await _mediator.Send(new GetPersonQuery(id));
    return StatusCode(200, person);
  }

  // POST api/<PersonsController>
  [HttpPost]
  [ProducesResponseType(typeof(int), StatusCodes.Status201Created)]
  [ProducesResponseType(StatusCodes.Status400BadRequest)]
  public async Task<ActionResult<int>> Post([FromBody] CreatePersonCommand person)
  {
    var result = await _mediator.Send(person);
    return StatusCode(201, result);
  }

  // PUT api/<PersonsController>/5
  [HttpPut("{id:int}")]
  [ProducesResponseType(typeof(int), StatusCodes.Status204NoContent)]
  [ProducesResponseType(StatusCodes.Status400BadRequest)]
  public async Task<ActionResult> Put(int id, [FromBody] UpdatePersonCommand person)
  {
    var result = await _mediator.Send(person);
    return StatusCode(204);
  }

  // DELETE api/<PersonsController>/5
  [HttpDelete("{id:int}")]
  [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
  [ProducesResponseType(StatusCodes.Status400BadRequest)]
  public async Task<ActionResult> Delete(int id)
  {
    var result = await _mediator.Send(new DeletePersonCommand { Id = id });
    return StatusCode(204);
  }
}